
package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
)


func TestTreeView_View(t *testing.T) {

	rootNode := NewTreeNode("Root")
	v := NewTreeView()
	v.Root = rootNode

	
	assert.True(t, v.IsInSight(rootNode))
}
