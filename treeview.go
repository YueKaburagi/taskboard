
package main

import (
	"github.com/gdamore/tcell/v2"	
	"github.com/rivo/tview"
	"code.rocketnine.space/tslocum/cbind"
)


type TreeNode struct {
	Reference interface{}
	children []*TreeNode
	parent *TreeNode
	Text string

	expanded bool

	hooksBlured []func(itself *TreeNode)
	hooksSelected []func(itself *TreeNode)

	textX int
}

func NewTreeNode(text string) *TreeNode {
	node := &TreeNode{
		Text: text,
		expanded: true,
	}
	return node
}

// 自身と同じ階層で次の Node を返す。なければ nil
func (n *TreeNode) NextSibling() *TreeNode {
	if n.parent == nil {
		return nil
	}
	itr := -2
	for i, x := range n.parent.children {
		if x == n {
			itr = i
		}
		if itr+1 == i {
			return x
		}
	}
	return nil
}

// 自身と同じ階層で前の Node を返す。なければ nil
func (n *TreeNode) PrevSibling() *TreeNode {
	if n.parent == nil {
		return nil
	}
	for i, x := range n.parent.children {
		if x == n {
			if i > 0 {
				return n.parent.children[i-1]
			} else {
				return nil
			}
		}
	}
	return nil
}

func (n *TreeNode) FirstChild() *TreeNode {
	if n.children == nil {
		return nil
	}
	return n.children[0]
}

func (n *TreeNode) LastChild() *TreeNode {
	if n.children == nil {
		return nil
	}
	return n.children[len(n.children)-1]
}

func (n *TreeNode) NextVisible() *TreeNode {
	var p *TreeNode
	if n.Expanded() {
		p = n.FirstChild()
	}
	if p == nil {
		p = n.NextSibling()
	}
	if p == nil {
		q := n.parent
		for q != nil {
			t := q.NextSibling()
			if t != nil {
				p = t
				return p
			}
			q = q.parent
		}
		if n.parent != nil {
			p = n.parent.NextSibling()
		}
	}
	return p
}

func (n *TreeNode) PrevVisible() *TreeNode {
	var p *TreeNode
	p = n.PrevSibling()
	if p == nil {
		return n.parent
	}
	for p.Expanded() {
		q := p.LastChild()
		if q == nil {
			return p
		}
		p = q
	}
	return p
}



// 行き掛け探索。再帰実装。破壊的変更への耐性はなし
func (n *TreeNode) Traverse(pred func(node *TreeNode)) {
	n.DroppableTraverse(func(node *TreeNode) bool {
		pred(node)
		return true
	})
}

// 述語が偽を返すと階層を降りない
func (n *TreeNode) DroppableTraverse(pred func(node *TreeNode) bool) {
	b := pred(n)
	if b {
		for _, x := range n.children {
			x.DroppableTraverse(pred)
		}
	}
}

// Note: Returns itself for convenience of tree construction.
func (n *TreeNode) AddLastChild(node *TreeNode) *TreeNode {
	// Cannot be child itself.
	if n == node {
		return n
	}
	node.parent = n
	n.children = append(n.children, node)
	return n
}

func (n *TreeNode) ClearChildren() {
	n.children = nil
}

func (n *TreeNode) RemoveChild(node *TreeNode) {
	itr := -1
	for i, x := range n.children {
		if x == node {
			itr = i
			break
		}
	}
	if itr != -1 {
		copy(n.children[itr:], n.children[itr+1:])
		length := len(n.children)
		n.children[length-1] = nil
		n.children = n.children[:length-1]
	}
}

func (n *TreeNode) Children() []*TreeNode {
	return n.children
}

func (n *TreeNode) Expanded() bool {
	return n.expanded
}

func (n *TreeNode) Expand() {
	n.expanded = true
}

func (n *TreeNode) Collapse() {
	n.expanded = false
}

func (n *TreeNode) ExpandAll() {
	n.Traverse(func(node *TreeNode) {
		node.Expand()
	})
}

// 関係ないけど tview のこの函数にバグありそう
func (n *TreeNode) CollapseAll() {
	n.Traverse(func(node *TreeNode) {
		node.Collapse()
	})
}

func (n *TreeNode) applyBlur() {
	for _, f := range n.hooksBlured {
		f(n)
	}
}

func (n *TreeNode) applySelect() {
	for _, f := range n.hooksSelected {
		f(n)
	}
}

func (n *TreeNode) AddHookBlur(hook func(itself *TreeNode)) {
	if hook == nil {
		return
	}
	n.hooksBlured = append(n.hooksBlured, hook)
}

func (n *TreeNode) AddHookSelect(hook func(itself *TreeNode)) {
	if hook == nil {
		return
	}
	n.hooksSelected = append(n.hooksSelected, hook)
}



type TreeView struct {
	*tview.Box
	
	Root *TreeNode
	current *TreeNode

	IsDrawRoot bool
	ColorStringForCurrentNode string
	IndentSize int

	actions map[string]Action
	keymap *cbind.Configuration

	nodesToDraw []*TreeNode
	offsetY int
}

func NewTreeView() *TreeView {
	v := &TreeView{
		Box: tview.NewBox(),

		IsDrawRoot: true,
		IndentSize: 3,

		keymap: cbind.NewConfiguration(),
	}

	v.actions = v.genActions()
	v.setDefaultKeymap()

	return v
}

func (v *TreeView) CurrentNode() *TreeNode {
	return v.current
}

func (v *TreeView) SetCurrentNode(node *TreeNode) {
	if node == nil {
		if v.current != nil {
			v.current.applyBlur()
			v.current = nil
		}
		return
	}
	v.refreshNodesToDraw()
	if !v.IsDrawRoot && node == v.Root {
		node = v.Root.FirstChild()
	}
	for _, x := range v.nodesToDraw {
		if x == node {
			if v.current != nil {
				v.current.applyBlur()
			}
			node.applySelect()
			v.current = node
		}
	}
}

func (v *TreeView) IsInSight(node *TreeNode) bool {
	v.refreshNodesToDraw()
	for i, x := range v.nodesToDraw {
		if i < v.offsetY {
			continue
		}
		_, _, _, height := v.GetInnerRect()
		if i > v.offsetY+height+1 {
			break
		}

		if x == node {
			return true
		}
	}
	return false
}

const (
	akLineUp = "moveUp"
	akLineDown = "moveDown"
	akPageUp = "movePageUp"
	akPageDown = "movePageDown"
	akJumpToTop = "moveToTop"
	akJumpToBottom = "moveToBottom"
)

func (v *TreeView) upkeep() {
	v.refreshNodesToDraw()
}

// withCurrent は current の移動を伴う場合は新しい current を返すこと
// current を変更するつもりがないなら nil か 元の current を返すこと
func (v *TreeView) WithCurrent(withCurrent func(current *TreeNode) (newCurrent *TreeNode), withoutCurrent func()) {
	if v.current == nil {
		if withoutCurrent != nil {
			withoutCurrent()
		}
		return 
	}
	if withCurrent == nil {
		return
	}
	old := v.current
	new := withCurrent(v.current)
	if new != nil && old != new {
		old.applyBlur()
		new.applySelect()
		v.current = new
	}
}

func (v *TreeView) fixOffsetY() {
	_, _, _, height := v.GetInnerRect()
	v.offsetY = Max(0, Min(len(v.nodesToDraw) - height, v.offsetY))
}

func (v *TreeView) upOffsetY(num int) {
	_, _, _, height := v.GetInnerRect()
	if v.current != nil {
		for i, x := range v.nodesToDraw {
			if x == v.current && (i > v.offsetY + height -1 || v.offsetY+1 > i) {
				v.offsetY = i - 1
				break
			}
		}
	} else {
		v.offsetY = v.offsetY - num
	}
	v.fixOffsetY()
}

func (v *TreeView) downOffsetY(num int) {
	_, _, _, height := v.GetInnerRect()
	if v.current != nil {
		for i, x := range v.nodesToDraw {
			if x == v.current && (i > v.offsetY + height -2 || v.offsetY > i) {
				v.offsetY = i - height + 2
			}
		}
	} else {
		v.offsetY = v.offsetY + num
	}
	v.fixOffsetY()
}

func (v *TreeView) genActions() map[string]Action {

	actions := make(map[string]Action)

	actions[akLineUp] = Action{
		Key: akLineUp,
		Description: "",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			v.WithCurrent(func(c *TreeNode) *TreeNode {
				return c.PrevVisible()
			}, func(){
				v.upOffsetY(1)
			})
			v.upOffsetY(0)
			return nil
		},
	}
	actions[akLineDown] = Action{
		Key: akLineDown,
		Description: "",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			v.WithCurrent(func(c *TreeNode) *TreeNode {
				return c.NextVisible()
			}, func(){
				v.downOffsetY(1)
			})
			v.downOffsetY(0)
			return nil
		},
	}
	actions[akPageUp] = Action{
		Key: akPageUp,
		Description: "",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			_, _, _, height := v.GetInnerRect()
			v.WithCurrent(func(c *TreeNode) *TreeNode {
				node := c
				r := node
				for i := 0; r != nil && i < height; i++ {
					r = node.PrevVisible()
					if r != nil {
						node = r
					}
				}
				return node
			}, func(){
				v.upOffsetY(height)
			})
			v.downOffsetY(0)
			return nil
		},
	}
	actions[akPageDown] = Action{
		Key: akPageDown,
		Description: "",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			_, _, _, height := v.GetInnerRect()
			v.WithCurrent(func(c *TreeNode) *TreeNode {
				node := c
				r := node
				for i := 0; r != nil && i < height; i++ {
					r = node.NextVisible()
					if r != nil {
						node = r
					}
				}
				return node
			}, func(){
				v.downOffsetY(height)
			})
			v.upOffsetY(0)
			return nil
		},
	}
	actions[akJumpToTop] = Action{
		Key: akJumpToTop,
		Description: "",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			v.offsetY = 0
			v.WithCurrent(func(current *TreeNode) (newCurrent *TreeNode) {
				if v.nodesToDraw != nil {
					return v.nodesToDraw[0]
				}
				return nil
			}, nil)
			return nil
		},
	}
	actions[akJumpToBottom] = Action{
		Key: akJumpToBottom,
		Description: "",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			_, _, _, height := v.GetInnerRect()
			l := len(v.nodesToDraw)
			v.offsetY = l - height
			v.fixOffsetY()
			v.WithCurrent(func(current *TreeNode) (newCurrent *TreeNode) {
				if v.nodesToDraw != nil {
					return v.nodesToDraw[l-1]
				}
				return nil
			}, nil)
			return nil
		},
	}

	return actions
}

func (v *TreeView) setDefaultKeymap() {
	v.keymap.Clear()

	v.keymap.SetRune( tcell.ModNone, 'j',           v.actions[akLineDown].Body)
	v.keymap.SetKey(  tcell.ModNone, tcell.KeyDown, v.actions[akLineDown].Body)
	v.keymap.SetRune( tcell.ModCtrl, 'n',           v.actions[akLineDown].Body)
	v.keymap.SetRune( tcell.ModNone, 'k',           v.actions[akLineUp].Body)
	v.keymap.SetKey(  tcell.ModNone, tcell.KeyUp,   v.actions[akLineUp].Body)
	v.keymap.SetRune( tcell.ModCtrl, 'p',           v.actions[akLineUp].Body)
	v.keymap.SetRune( tcell.ModNone, ' ',           v.actions[akPageDown].Body)
	v.keymap.SetKey(  tcell.ModNone, tcell.KeyPgDn, v.actions[akPageDown].Body)
	v.keymap.SetRune( tcell.ModCtrl, 'v',           v.actions[akPageDown].Body)
	v.keymap.SetRune( tcell.ModNone, 'b',           v.actions[akPageUp].Body)
	v.keymap.SetKey(  tcell.ModNone, tcell.KeyPgUp, v.actions[akPageUp].Body)
	v.keymap.SetRune( tcell.ModCtrl, 'z',           v.actions[akPageUp].Body)
	v.keymap.SetRune( tcell.ModNone, 'g',           v.actions[akJumpToTop].Body)
	v.keymap.SetRune( tcell.ModNone, 'G',           v.actions[akJumpToBottom].Body)
}

func (v *TreeView) AllDefinedActions() map[string]Action {
	return v.actions
}

func (v *TreeView) refreshNodesToDraw() {
	v.nodesToDraw = nil
	if v.Root == nil {
		return
	}

	v.Root.DroppableTraverse(func(node *TreeNode) bool {
		if node.parent == nil {
			node.textX = 0
		} else {
			node.textX = node.parent.textX + v.IndentSize
			if !v.IsDrawRoot && v.Root == node.parent {
				node.textX = node.parent.textX
			}
		}

		if v.IsDrawRoot || v.Root != node {
			v.nodesToDraw = append(v.nodesToDraw, node)
		}

		return node.Expanded()
	})
}

func (v *TreeView) Draw(screen tcell.Screen) {
	v.Box.DrawForSubclass(screen, v)

	v.upkeep()

	_, screenHeight := screen.Size()
	x, y, width, height := v.GetInnerRect()

	posY := y
	for ix, node := range v.nodesToDraw {
		if posY >= y+height+1 || posY >= screenHeight {
			break
		}
		if ix < v.offsetY {
			continue
		}

		if v.ColorStringForCurrentNode != "" && v.current == node {
			text := v.ColorStringForCurrentNode + node.Text
			tview.Print(screen, text, x+node.textX, posY, width-node.textX, tview.AlignLeft, tcell.ColorWhite)
		} else {
			tview.Print(screen, node.Text, x+node.textX, posY, width-node.textX, tview.AlignLeft, tcell.ColorWhite)
		}

		posY += 1
	}
}

func (v *TreeView) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return v.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		v.keymap.Capture(event)
	})
}

func (v *TreeView) MouseHandler() func(action tview.MouseAction, event *tcell.EventMouse, setFocus func(p tview.Primitive)) (consumed bool, capture tview.Primitive) {
	return v.WrapMouseHandler(func(action tview.MouseAction, event *tcell.EventMouse, setFocus func(p tview.Primitive)) (consumed bool, capture tview.Primitive) {
		x, y := event.Position()
		if !v.InRect(x,y) {
			return false, nil
		}

		switch action {
		case tview.MouseLeftClick:
			setFocus(v)
			consumed = true
		}

		return
	})
}
