
package main

import (
	"fmt"
	"math"
	"time"
	"github.com/rivo/tview"
)

type PriorityLevel int

type Priority struct {
	Level PriorityLevel
	Name string
}

type Weight struct {
	Weight int
	Name string
}

type Tag struct {
	Name string `json:"name"`
	AlterName string `json:"alter"`
	// ShortHand string
}

func NewSimpleTag(name string) *Tag {
	return &Tag{
		Name: name,
		AlterName: name,
	}
}

type TaskState int
type TaskType int

const (
	TtNormal TaskType = iota
	TtPseudo
	TtInfinity
)

func (t TaskType) String() string {
	switch t {
	case TtInfinity:
		return "Infinity"
	case TtPseudo:
		return "Pseudo"
	default:
		return "Normal"
	}
}

type Task struct {
	Name string
	Tags []*Tag
	CreatedTime time.Time
	DoneTime *time.Time
	Deadline *time.Time
	EffortTime *time.Time
	Priority *Priority
	Type TaskType
	State TaskState
	Note *string
	Times int // Infinite Task で利用。done 回数をカウントする
}

func (t Task) HasTag(name string) bool {
	ret := false
	for _, x := range t.Tags {
		if x.Name == name {
			ret = true
			break
		}
	}
	return ret
}

func (t *Task) SetDone() *Task {
	switch t.Type {
	case TtNormal:
		t.State = StateDone
		time := time.Now()
		t.DoneTime = &time
	case TtInfinity:
		t.Times += 1
	}

	return nil
}

// タイプ変更の結果、別Taskが生成された場合に non-nil を返す
func (t *Task) SetType(newType TaskType) *Task {
	if t.Type == newType {
		return nil
	}
	switch t.Type {
	case TtInfinity:
		if t.Times > 0 {
			alter := t.Clone()
			alter.Type = newType
			alter.Times = 0
			t.State = StateClosed
			return alter
		}
	default:
		t.Type = newType
	}

	return nil
}

func clone_ptime_shallow(t *time.Time) *time.Time {
	if t != nil {
		bind := *t
		return &bind
	}
	return t
}

func clone_pstring_shallow(s *string) *string {
	if s != nil {
		bind := *s
		return &bind
	}
	return s
}

func (t *Task) Clone() *Task {
	tags := make([]*Tag, len(t.Tags))
	for i, x := range t.Tags {
		tags[i] = x
	}
	cTime := time.Now()
	doneTime := clone_ptime_shallow(t.DoneTime)
	deadline := clone_ptime_shallow(t.Deadline)
	effortTime := clone_ptime_shallow(t.EffortTime)
	note := clone_pstring_shallow(t.Note)
	nt := Task{
		Name: t.Name,
		Tags: tags,
		CreatedTime: cTime,
		DoneTime: doneTime,
		Deadline: deadline,
		EffortTime: effortTime,
		Priority: t.Priority, // Priority の実体は Known に依存する
		Type: t.Type,
		State: t.State,
		Note: note,
		Times: t.Times,
	}
	return &nt
}

func genBlocks(cfg *Configuration, task *Task) []ColoredBlock {
	result := []ColoredBlock{}

	if task.Priority != nil {
		prio := ColoredBlock{ Text: tview.Escape("[" + task.Priority.Name + "]"), FGColor: "navy" }
		result = append(result, prio)
	}

	var tb ColoredBlock
	if task.Deadline != nil {
		tvi, isFuture := genRestTime(task.Deadline)
		var color string
		if isFuture {
			color = "maroon"
		} else {
			color = "red"
		}
		tb = ColoredBlock{ Text: tvi, FGColor: color }
	} else if task.EffortTime != nil {
		tvi, _ := genRestTime(task.EffortTime)
		tb = ColoredBlock{ Text: tvi, FGColor: "green" }
	}
	result = append(result, tb)

	if task.Type == TtInfinity {
		s := fmt.Sprintf("%d", task.Times)
		count := ColoredBlock{ Text: s, FGColor: "teal" }
		result = append(result, count)
	}

	note := ColoredBlock{ Text: "", FGColor: "green" }
	if task.Note != nil {
		note.Text = string(cfg.NoteIcon)
		result = append(result, note)
	}

	name := ColoredBlock{ Text: tview.Escape(task.Name), FGColor: "olive" }
	result = append(result, name)

	tags := make([]ColoredBlock, len(task.Tags))
	for i, t := range task.Tags {
		tags[i] = ColoredBlock{ Text: "#" + t.AlterName, FGColor: "gray" }
	}
	result = append(result, tags...)

	return result
}

/**
 * Return translated string from time difference between `t` and now.
 * Secondary return true if `t` is now or future, or false otherwise.
 */
func genRestTime(t *time.Time) (string, bool){
	now := time.Now()

	s := ""
	isFuture := true
	if t != nil {
		diff := t.Sub(now)
		mins := diff.Minutes()
		isFuture = mins >= 0
		hours := mins / 60
		if -60.0 < mins && mins < 60.0 {
			s = fmt.Sprintf("%dm", int(math.Floor(mins)))
		} else if -48 < hours && hours < 48 {
			s = fmt.Sprintf("%dh", int(hours))
		} else {
			days := math.Floor(hours / 24)
			s = fmt.Sprintf("%dd", int(days))
		}
	}
	return s, isFuture
}





type WorkTask struct {
	Tags []*Tag
	Priority *Priority
	Deadline *time.Time
	EffortTime *time.Time
	Type TaskType
	Note string
}

const (
	StateNormal TaskState = iota
	StateDone
	StateFrozen
	StateClosed
)

func (s TaskState) String() string {
	switch s {
	case StateClosed:
		return "Closed"
	case StateFrozen:
		return "Frozen"
	case StateDone:
		return "Done"
	default:
		return "Normal"
	}
}
