
package main

import (
	"io"
	"os"
	"log"
	"os/exec"
	"encoding/json"
	"github.com/rivo/tview"
)

func Max(a,b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func Min(a,b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

	// ColorBlack = ColorValid + iota
	// ColorMaroon
	// ColorGreen
	// ColorOlive
	// ColorNavy
	// ColorPurple
	// ColorTeal
	// ColorSilver
	// ColorGray
	// ColorRed
	// ColorLime
	// ColorYellow
	// ColorBlue
	// ColorFuchsia
	// ColorAqua
	// ColorWhite

type WrappedTask struct {
	*Task
	*BlockManager
}

func (w *WrappedTask) GenDecolatedText() string {
	return "[silver]+[-:black] " + w.BlockManager.GenDecolatedText()
}

func (w *WrappedTask) ReCalc(cfg *Configuration) *WrappedTask{
	w.SetBlocks(genBlocks(cfg, w.Task))
	w.SetCursor(w.GetCursor())
	return w
}

func NewWrappedTask(cfg *Configuration, task *Task) *WrappedTask {
	w := &WrappedTask{
		Task: task,
		BlockManager: NewBlockManager(),
	}
	w.SetBlocks(genBlocks(cfg, task))
	return w
}


const (
	LLMinimum LogLevel = iota
	LLVerbose
	LLDebug
)
type LogLevel int

var logLevel LogLevel
var logger *log.Logger

func LogInfo(level LogLevel, v interface{}) {
	if level <= logLevel {
		logger.Println(v)
	}
}

func main() {
	logLevel = LLDebug
	logFile, err := os.OpenFile("taskboard.log", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
	defer logFile.Close()
	if err != nil { panic(err) }
	logger = log.New(logFile, "", log.Ldate|log.Ltime|log.Lshortfile)

	app := tview.NewApplication()

	cfg := NewMockConfiguration()
	known, err := LoadOrUseDefault("known.json")
	if err != nil {
		logger.Panic(err)
	}
	store, err := LoadTaskStore("taskboard.json")
	store.RefreshTags(known)

	cTasks := NewTaskListScene()
	cInput := NewEditTaskScene()

	saveAll := func() {
		store.SaveTo("taskboard.json")
		known.SaveTo("known.json")
	}

	quitd := func() {
		blob, err := json.Marshal(known.Tags)
		if err != nil {
			logger.Panic(err)
		}
		LogInfo(LLDebug, string(blob))
	}

	modeInput := func(t *Task) {
		cInput.Reset(func(p tview.Primitive){ app.SetFocus(p) }, t)
		app.SetRoot(cInput.GetScene(), true)
	}
	modeView := func() {
		app.SetRoot(cTasks.GetScene(), true)
	}


	tmpPath := os.TempDir() + string(os.PathSeparator) + "TASKBOARD_NOTE"
	genCmd := func() *exec.Cmd {
		cmd := exec.Command("nano", tmpPath)
		// Bypass stdio.
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		return cmd
	}
	// Reserve to remove temporary file.
	defer os.Remove(tmpPath)
	openNote := func(init string, callback func(s string)) {
		app.Suspend(func() {
			// Create/Truncate temporary file. And write `init`.
			file0, err := os.Create(tmpPath)
			if err != nil { logger.Panic(err) }
			_, err = file0.WriteString(init)
			if err != nil { logger.Panic(err) }
			err = file0.Close()
			if err != nil { logger.Panic(err) }

			// Open via nano
			cmd := genCmd()
			err = cmd.Run() // DO NOT REUSE
			if err != nil { logger.Panic(err) }

			// Read updated note.
			file, err := os.OpenFile(tmpPath, os.O_RDONLY|os.O_CREATE, 0666)
			if err != nil { logger.Panic(err) }
			bytes, err := io.ReadAll(file)
			if err != nil { logger.Panic(err) }
			str := string(bytes)
			file.Close()
			callback(str)
		})
	}

	cTasks.SetConfiguration(cfg)
	cTasks.SetTaskStore(store)
	cTasks.SetSwitchToAddNewTaskScene(modeInput)
	cTasks.SetQuitCallback(func(){ app.Stop(); quitd() })
	cTasks.SetKnown(known)
	cInput.SetAddNewTaskCallback(func(task *Task) {
		cTasks.AddTask(task)
		saveAll()
		modeView()
	})
	cInput.SetEditTaskCallback(func(old *Task, new *Task) {
		cTasks.SetTask(old,new)
		saveAll()
		modeView()
	})
	cInput.SetKnownSources(known)
	cInput.SetConfigurationSource(cfg)
	cInput.SetCallbackOpenNote(openNote)
	cInput.SetCancelCallback(modeView)

	modeView()
	err = app.Run()
	if err != nil {
		logger.Panic(err)
	}
	saveAll()


}
