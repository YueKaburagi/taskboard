
package main

import (
	"github.com/rivo/tview"
)

type Capsule struct {
	Primitive tview.Primitive
	HookFocus func()
	HookBlur func()
}
func NewSimpleCapsule(p tview.Primitive) Capsule {
	return Capsule{Primitive: p}
}

type MiniRing struct {
	next, prev *MiniRing
	Value Capsule
}

func MiniRingSingleton(c Capsule) *MiniRing {
	r := &MiniRing{
		next: nil,
		prev: nil,
		Value: c,
	}
	r.next = r
	r.prev = r
	return r
}

func MiniRingFromSlice(cs []Capsule) *MiniRing {
	var r *MiniRing = nil
	for _, c := range cs {
		if r == nil {
			r = MiniRingSingleton(c)
		} else {
			r.AddNext(c)
			r = r.NextSlot()
		}
	}
	if r != nil {
		r = r.NextSlot()
	}
	return r
}

func (r *MiniRing) AddNext(c Capsule) *MiniRing {
	q := MiniRingSingleton(c)
	q.prev = r
	q.next = r.next
	r.next.prev = q
	r.next = q
	return r
}

func (r *MiniRing) NextSlot() *MiniRing {
	return r.next
}

func (r *MiniRing) PrevSlot() *MiniRing {
	return r.prev
}

func (r *MiniRing) FindForward(p tview.Primitive) *MiniRing {
	beg := r
	first := true
	var q *MiniRing
	for q = beg; first || beg != q; q = q.next {
		if q.Value.Primitive == p {
			break
		}
		if beg == q { first = false }
	}
	return q
}

func (r *MiniRing) ForEach(f func(c Capsule)) {
	beg := r
	first := true
	var q *MiniRing
	for q = beg; first || beg != q; q = q.next {
		f(q.Value)
		if beg == q { first = false }
	}
}
