
package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/rivo/tview"
)

func TestSingleton_NextPrev(t *testing.T) {
	p := tview.NewTextView()
	r := MiniRingSingleton(p)

	assert.Equal(t, r, r.NextSlot())
	assert.Equal(t, r, r.PrevSlot())
	assert.Equal(t, r.NextSlot(), r.PrevSlot())
}

func TestFromSlice_Basic(t *testing.T) {
	p1 := tview.NewTextView()
	p2 := tview.NewInputField()
	p3 := tview.NewDropDown()
	p4 := tview.NewTreeView()

	r := MiniRingFromSlice([]tview.Primitive{p1,p2,p3,p4})

	assert.Equal(t, r.NextSlot().Value, p2)
	assert.Equal(t, r.PrevSlot().Value, p4)
	assert.Equal(t, r.NextSlot().NextSlot(), r.PrevSlot().PrevSlot())
	assert.Equal(t, r.NextSlot().PrevSlot().Value, p1)
}
