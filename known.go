
package main

import (
	"os"
	"strings"
	"encoding/json"
)

type Known struct {
	Priorities []*Priority
	Tags []*Tag
	Weights []*Weight
}

func NewMockKnown() *Known {
	pPack := []*Priority{
		{Level: 1, Name: "High"},
		{Level: 2, Name: "Mid"},
		{Level: 3, Name: "Low"},
	}
	tPack := []*Tag{
		{Name: "rabbit", AlterName: "うさぎ"},
		{Name: "crab", AlterName: "かに"},
	}
	wPack := []*Weight{
		{Weight: 34, Name: "Leviathan"},
		{Weight: 21, Name: "Wyrm"},
		{Weight: 13, Name: "Cyclops"},
		{Weight: 8, Name: "Girtablulu"},
		{Weight: 5, Name: "Fairy"},
		{Weight: 3, Name: "Tentacle"},
	}

	return &Known{
		Priorities: pPack,
		Tags: tPack,
		Weights: wPack,
	}
}

func (k *Known) FilterTags(filter func(t *Tag) bool) []*Tag {
	tags := []*Tag{}
	for _, t := range k.Tags {
		if filter(t) {
			tags = append(tags, t)
		}
	}
	return tags
}

// 1文字以上で発火、AlterNameについては無視する
func (k *Known) HandlerAutocompleteTag(part string) []string {
	if len(part) == 0 {
		return nil
	}

	tags := k.FilterTags(func(t *Tag) bool {
		return strings.Contains(t.Name, part)
	})
	strs := make([]string, len(tags))
	for i, t := range tags {
		strs[i] = t.Name
	}
	return strs
}

func (k *Known) LookupOrCreateTag(name string) *Tag {
	var tag *Tag = nil
	for _, t := range k.Tags {
		if t.Name == name {
			tag = t
			break
		}
	}
	if tag == nil {
		t := NewSimpleTag(name)
		tag = t
		k.Tags = append(k.Tags, tag) // within clone.
	}
	return tag
}

// return nullable *Priority
func (k *Known) LookupPriority(name string) *Priority {
	var prio *Priority = nil
	for _, p := range k.Priorities {
		if p.Name == name {
			prio = p
			break
		}
	}
	return prio
}

func (k *Known) SaveTo(filename string) error {
	file, err := os.OpenFile(filename, os.O_WRONLY | os.O_CREATE, 0644)
	if err != nil {
		LogInfo(LLMinimum, "Cannot open file. [" + filename + "].")
		return err
	}
	encoder := json.NewEncoder(file)
	return encoder.Encode(k)
}

func LoadOrUseDefault(filename string) (*Known, error) {
	file, err := os.OpenFile(filename, os.O_RDONLY, 0644)
	if err != nil {
		if os.IsNotExist(err) {
			return NewMockKnown(), nil
		} else {
			return nil, err
		}
	} else {
		var known Known
		decoder := json.NewDecoder(file)
		err := decoder.Decode(&known)
		return &known, err
	}
}
