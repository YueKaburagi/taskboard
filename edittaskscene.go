
package main

import (
	"time"
	"strings"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"code.rocketnine.space/tslocum/cbind"
)

type EditTaskScene struct {
	pFlexBox *tview.Flex
	inputRing *MiniRing

	target *Task
	workTask WorkTask
	tagsBM *BlockManager

	pNameInput *tview.InputField
	pTagsView *tview.TextView
	pTagInput *tview.InputField
	pPrioSelector *tview.DropDown
	pDeadlineView *tview.TextView
	pEffortTimeView *tview.TextView
	pTimeInput *tview.InputField
	pTypeSelector *tview.DropDown
	pNoteView *tview.TextView
	pNoteEditButton *tview.TextView

	cfg *Configuration
	known *Known

	callbackAddNewTask func(task *Task)
	callbackEditTask func(old *Task, new *Task)
	callbackCancel func()
	callbackOpenNote func(init string, callback func(s string))
	delegateSetFocus func(p tview.Primitive)

	keymap *cbind.Configuration
	keymapTagsView *cbind.Configuration
	keymapTagInput *cbind.Configuration
}

func (a *EditTaskScene) genTagBlocks() []ColoredBlock {
	tags := a.workTask.Tags
	blocks := make([]ColoredBlock, len(tags))
	for i, t := range tags {
		blocks[i] = ColoredBlock{
			Text: t.Name,
			FGColor: "lime",
			Style: "b",
			Reference: t,
		}
	}
	return blocks
}

func (a *EditTaskScene) createTask() {
	name := a.pNameInput.GetText()
	var note *string
	if a.workTask.Note != "" {
		// WorkTask の mamber で Note だけ実体なせいで、
		// lifetime が WorkTask に依存してしまうのをここに束縛することで回避
		bind := a.workTask.Note
		note = &bind
	}
	a.inputRing.ForEach(func(c Capsule) {
		if c.HookBlur != nil {
			c.HookBlur()
		}
	})
	if a.target == nil {
		if a.callbackAddNewTask != nil {
			a.callbackAddNewTask(&Task{
				Name: name,
				Tags: a.workTask.Tags,
				CreatedTime: time.Now(),
				Deadline: a.workTask.Deadline,
				EffortTime: a.workTask.EffortTime,
				Priority: a.workTask.Priority,
				Type: a.workTask.Type,
				Note: note,
			})
		}
	} else {
		if a.callbackEditTask != nil {
			a.callbackEditTask(a.target, &Task{
				Name: name,
				Tags: a.workTask.Tags,
				CreatedTime: a.target.CreatedTime,
				Deadline: a.workTask.Deadline,
				EffortTime: a.workTask.EffortTime,
				Priority: a.workTask.Priority,
				Type: a.workTask.Type,
				State: a.target.State,
				Times: a.target.Times,
				Note: note,
			})
			
		}
	}
}

func (a *EditTaskScene) reflectDeadline() {
	if a.workTask.Deadline == nil {
		a.pDeadlineView.SetText("Deadline: (none)" )
	} else {
		// Format で使えるパーツについては time.format.go のコメントを読むこと
		formatted := a.workTask.Deadline.Format("1/2 15:04")
		a.pDeadlineView.SetText("Deadline: " + formatted)
	}
}

func (a *EditTaskScene) reflectEffortTime() {
	et := a.workTask.EffortTime
	if et == nil {
		a.pEffortTimeView.SetText("EffortTime: (none)")
	} else {
		formatted := et.Format("1/2 15:04")
		a.pEffortTimeView.SetText("EffortTime: " + formatted)
	}
}

func (a *EditTaskScene) reflectNote() {
	str := a.workTask.Note
	a.pNoteView.SetText(str)
}

// Export for test.
func ParseTime(str string) *time.Time {
	patterns := []string{"1/2", "1/2 15:", "1/2 15:04",
		"2006/1/2", "2006/1/2 15:", "2006/1/2 15:04"}

	var t time.Time
	var err error
	for _, pat := range patterns {
		t, err = time.Parse(pat, str)
		if err == nil {
			if t.Year() == 0 {
				now := time.Now()
				t = t.AddDate(now.Year(), 0, 0)
				if now.After(t) { // now > t
					t = t.AddDate(1, 0, 0)
				}
			}
			break
		}
	}

	if err == nil {
		return &t
	} else {
		return nil
	}
}

func parseDuration(str string) *time.Time {
	d, err := time.ParseDuration(str)

	if err == nil {
		now := time.Now()
		t := now.Add(d)
		return &t
	} else {
		return nil
	}
}

func (a *EditTaskScene) setTimeFromString(str string) {

	setAsDeadline := func(t *time.Time) {
		a.workTask.Deadline = t
		a.reflectDeadline()
	}
	setAsEffortTime := func(t *time.Time) {
		a.workTask.EffortTime = t
		a.reflectEffortTime()
	}

	var setter func(t *time.Time)
	var s string
	if strings.HasPrefix(str, "d ") {
		s = strings.TrimPrefix(str, "d ")
		setter = setAsDeadline
	} else if strings.HasPrefix(str, "e ") {
		s = strings.TrimPrefix(str, "e ")
		setter = setAsEffortTime
	} else {
		return
	}

	var t *time.Time
	if strings.HasPrefix(s, "in ") {
		t = parseDuration(strings.TrimPrefix(s, "in "))
	} else {
		t = ParseTime(s)
	}

	if t != nil {
		setter(t)
	} else if s == "" {
		setter(nil)
	}
}

func NewEditTaskScene() *EditTaskScene {
	pFlex := tview.NewFlex().SetDirection(tview.FlexRow)
	pNameInput := tview.NewInputField().SetLabel("Name:")
	pTagsView := tview.NewTextView().
		SetDynamicColors(true). // Enable color tags. (TextView ignoring color tags in default.)
		SetWordWrap(true)
	pTagInput := tview.NewInputField().SetLabel("Add Tag:")
	pPrioInput := tview.NewDropDown().SetLabel("Priority:")

	pDeadlineView := tview.NewTextView().SetWordWrap(true).SetText("Deadline: ")
	pEffortTimeView := tview.NewTextView().SetWordWrap(true).SetText("EffortTime: ")
	pTimeInput := tview.NewInputField().SetLabel("Set Time:")

	pTypeSelector := tview.NewDropDown().SetLabel("Type:")

	pNoteView := tview.NewGrid().SetRows(1,1,1).SetColumns(6,0,6).SetBorders(false)
	pNoteViewLabel := tview.NewTextView().SetText("Note:")
	pNoteViewSummary := tview.NewTextView()
	pNoteViewButton := tview.NewTextView().SetDynamicColors(true).SetText("[-:gray]Edit").SetTextAlign(tview.AlignCenter)
	pNoteView.AddItem(pNoteViewLabel, 0, 0, 1, 1, 1, 5, false)
	pNoteView.AddItem(pNoteViewButton, 2, 2, 1, 1, 1, 4, false)
	pNoteView.AddItem(pNoteViewSummary, 0, 1, 3, 1, 1, 0, false)

	pFlex.AddItem(pNameInput, 0, 1, true) // Default focus
	pFlex.AddItem(pPrioInput, 0, 1, false)
	pFlex.AddItem(pDeadlineView, 0, 1, false)
	pFlex.AddItem(pEffortTimeView, 0, 1, false)
	pFlex.AddItem(pTimeInput, 0, 1, false)
	pFlex.AddItem(pTagsView, 0, 1, false)
	pFlex.AddItem(pTagInput, 0, 1, false)
	pFlex.AddItem(pTypeSelector, 0, 1, false)
	pFlex.AddItem(pNoteView, 0, 1, false)

	a := &EditTaskScene{
		pFlexBox: pFlex,
		inputRing: nil,

		workTask: WorkTask{},
		target: nil,

		pNameInput: pNameInput,
		tagsBM: NewBlockManager(),
		pTagsView: pTagsView,
		pTagInput: pTagInput,
		pPrioSelector: pPrioInput,
		pDeadlineView: pDeadlineView,
		pEffortTimeView: pEffortTimeView,
		pTimeInput: pTimeInput,
		pTypeSelector: pTypeSelector,

		pNoteView: pNoteViewSummary,
		pNoteEditButton: pNoteViewButton,

		cfg: nil,
		known: nil,

		callbackAddNewTask: func(_t *Task){},
		callbackEditTask: nil,
		callbackCancel: func(){},
		delegateSetFocus: nil,

		keymap: cbind.NewConfiguration(),
		keymapTagsView: cbind.NewConfiguration(),
		keymapTagInput: cbind.NewConfiguration(),
	}
	a.updateTags() // Set label.

	// Lazy init
	rInputsPack := []Capsule{
		NewSimpleCapsule(pNameInput),
		NewSimpleCapsule(pPrioInput),
		NewSimpleCapsule(pTimeInput),
		{
			Primitive: pTagsView,
			HookFocus: func() {
				a.tagsBM.SetCursor(0)
				a.updateTags()
			},
			HookBlur: func() {
				a.tagsBM.SetCursor(-1)
				a.updateTags()
			},
		},
		NewSimpleCapsule(pTagInput),
		NewSimpleCapsule(pTypeSelector),
		{
			Primitive: pNoteViewSummary,
			HookFocus: func() {
				pNoteViewButton.SetText("[black:lime]Edit")
			},
			HookBlur: func() {
				pNoteViewButton.SetText("[-:gray]Edit")
			},
		},
	}
	rInputs := MiniRingFromSlice(rInputsPack)
	a.inputRing = rInputs


	a.setKeymap()
	a.pFlexBox.SetInputCapture(a.keymap.Capture)
	a.pTagsView.SetInputCapture(a.keymapTagsView.Capture)
	a.pTagInput.SetInputCapture(a.keymapTagInput.Capture)

	a.pTagInput.SetAutocompleteFunc(func (str string) []string {
		if a.known != nil {
			return a.known.HandlerAutocompleteTag(str)
		} else {
			return nil
		}
	})

	nextPrevInput := func(key tcell.Key) {
		updateFocus := func(old Capsule) {
			v := a.inputRing.Value
			if old.Primitive != v.Primitive {
				a.delegateSetFocus(v.Primitive)
				if old.HookBlur != nil {
					old.HookBlur()
				}
				if v.HookFocus != nil {
					v.HookFocus()
				}
			}
		}
		switch key {
		case tcell.KeyTab:
			if a.delegateSetFocus != nil {
				o := a.inputRing.Value
				a.inputRing = a.inputRing.NextSlot()
				updateFocus(o)
			}
		case tcell.KeyBacktab:
			if a.delegateSetFocus != nil {
				o := a.inputRing.Value
				a.inputRing = a.inputRing.PrevSlot()
				updateFocus(o)
			}
		}
	}


	a.pNameInput.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyEnter:
			a.createTask()
		case tcell.KeyEscape:
			a.callbackCancel()
		default:
			nextPrevInput(key)
		}
	})

	a.pPrioSelector.SetDoneFunc(func(key tcell.Key) {
		switch key {
			//		case tcell.KeyEscape:
		default:
			nextPrevInput(key)
		}
	})

	a.pTimeInput.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyEnter:
			a.setTimeFromString(a.pTimeInput.GetText())
			a.pTimeInput.SetText("")
		default:
			nextPrevInput(key)
		}
	})

	a.pTagsView.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyTab:
			nextPrevInput(key)
		case tcell.KeyBacktab:
			nextPrevInput(key)
		}
	})
	
	a.pTagInput.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyEnter:
			tagname := a.pTagInput.GetText()
			if len(tagname) > 0 {
				tag := a.known.LookupOrCreateTag(tagname)
				already := false
				for _, t := range a.workTask.Tags {
					if t.Name == tag.Name {
						already = true
						break
					}
				}
				if !already {
					a.workTask.Tags = append(a.workTask.Tags, tag)
					// append creates new instance.
					a.updateTags()
				}
				a.pTagInput.SetText("")

			}
		case tcell.KeyEscape:
			a.pTagInput.SetText("")
		default:
			nextPrevInput(key)
		}
	})

	pTypeSelector.SetDoneFunc(nextPrevInput)

	pNoteViewSummary.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyEnter:
			if a.callbackOpenNote != nil {
				a.callbackOpenNote(a.workTask.Note, func(s string) {
					a.workTask.Note = s
					a.reflectNote()
				})
			}
		default:
			nextPrevInput(key)
		}
	})


	a.reflectDeadline()
	a.reflectEffortTime()

	return a
}

func (a *EditTaskScene) setKeymap() *EditTaskScene {
	a.keymap.Clear()
	a.keymapTagsView.Clear()
	a.keymapTagInput.Clear()

	// 制御文字で表現される Backspace, Tab, Escape, Enter, Backspace2 などは
	// tcell が ascii文字 として入力を受け取る関係で、
	// BS:0x08, HT:0x09, ESC:0x1B, CR:0x0D, DEL:0x7F として扱われるため
	// +0x20 する Shift と -0x40 する Ctrl は都合使用できない
	a.keymap.SetKey(tcell.ModAlt, tcell.KeyEnter, func(ev *tcell.EventKey) *tcell.EventKey {
		a.createTask()
		return nil // Consume key.
	})

	a.keymapTagsView.SetRune(tcell.ModNone, 'l', func(ev *tcell.EventKey) *tcell.EventKey {
		a.tagsBM.SetCursor(a.tagsBM.CursorSucc())
		a.updateTags()
		return nil // Consume key.
	})
	a.keymapTagsView.SetRune(tcell.ModNone, 'h', func(ev *tcell.EventKey) *tcell.EventKey {
		a.tagsBM.SetCursor(a.tagsBM.CursorPred())
		a.updateTags()
		return nil // Consume key.
	})

	removeTag := func(ev *tcell.EventKey) *tcell.EventKey {
		a.tagsBM.UseCurrentReference(func(ref interface{}) {
			nts := make([]*Tag, len(a.workTask.Tags)-1)
			i := 0
			at := ref.(*Tag)
			for _, t := range a.workTask.Tags {
				if t != at {
					nts[i] = t
					i = i + 1
				}
			}
			a.workTask.Tags = nts
			a.updateTags()
		})
		return nil
	}
	a.keymapTagsView.SetRune(tcell.ModNone, 'd', removeTag)
	a.keymapTagsView.SetKey(tcell.ModNone, tcell.KeyDelete, removeTag)

	return a
}


func (a *EditTaskScene) GetScene() tview.Primitive {
	return a.pFlexBox
}


func (a *EditTaskScene) updateTags() *EditTaskScene {
	a.tagsBM.SetBlocks(a.genTagBlocks())
	a.pTagsView.SetText("Tags: " + a.tagsBM.GenDecolatedText())
	return a
}

func (a *EditTaskScene) Reset(delegate func(p tview.Primitive), target *Task) *EditTaskScene {
	a.delegateSetFocus = delegate
	a.target = target
	if target == nil {
		a.pNameInput.SetText("")
		a.workTask = WorkTask{}
		a.resetPriority(nil)
		a.resetTaskType(TtNormal)
		a.updateTags()
		a.reflectDeadline()
		a.reflectEffortTime()
		a.reflectNote()
	} else {
		a.pNameInput.SetText(target.Name)
		a.workTask = WorkTask{}
		a.resetPriority(target.Priority)
		a.resetTaskType(target.Type)
		a.workTask.Tags = make([]*Tag, len(target.Tags))
		copy(a.workTask.Tags, target.Tags)
		a.updateTags()
		a.workTask.Deadline = target.Deadline
		a.reflectDeadline()
		a.workTask.EffortTime = target.EffortTime
		a.reflectEffortTime()
		if target.Note != nil {
			a.workTask.Note = *target.Note
		}
		a.reflectNote()
	}
	a.pTagInput.SetText("")
	a.inputRing = a.inputRing.FindForward(a.pNameInput)
	return a
}

func (a *EditTaskScene) SetAddNewTaskCallback(callback func(task *Task)) *EditTaskScene {
	a.callbackAddNewTask = callback
	return a
}

func (a *EditTaskScene) SetEditTaskCallback(callback func(old *Task, new *Task)) *EditTaskScene {
	a.callbackEditTask = callback
	return a
}

func (a *EditTaskScene) SetCancelCallback(callback func()) *EditTaskScene {
	a.callbackCancel = callback
	return a
}

func (a *EditTaskScene) resetPriority(prio *Priority) {
	if prio == nil {
		if a.cfg != nil && a.known != nil {
			prio = a.cfg.GetDefaultPriority(a.known)
		}
	}
	if a.known != nil && prio != nil {
		// Clear all options.
		a.pPrioSelector.SetOptions(nil,nil)

		for i, p := range a.known.Priorities {
			// Bind *Priority locally.
			// 'p' is declared out of for expression.
			x := p

			a.pPrioSelector.AddOption(x.Name, func(){
				a.workTask.Priority = x
			})
			if x.Level == prio.Level {
				a.workTask.Priority = x
				a.pPrioSelector.SetCurrentOption(i)
			}
		}
	}
}

func (a *EditTaskScene) resetTaskType(t TaskType) {
	// Clear all options.
	a.pTypeSelector.SetOptions(nil, nil)

	switch t {
	case TtPseudo:
		a.pTypeSelector.AddOption(TtPseudo.String(), func(){
			a.workTask.Type = TtPseudo
		})
		a.pTypeSelector.SetCurrentOption(0)
	default:
		a.pTypeSelector.AddOption(TtNormal.String(), func(){
			a.workTask.Type = TtNormal
		})
		a.pTypeSelector.AddOption(TtInfinity.String(), func(){
			a.workTask.Type = TtInfinity
		})
		a.pTypeSelector.SetCurrentOption(0)
		if t == TtInfinity {
			a.pTypeSelector.SetCurrentOption(1)
		}
	}
}

func (a *EditTaskScene) SetConfigurationSource(c *Configuration) *EditTaskScene {
	a.cfg = c
	return a
}

func (a *EditTaskScene) SetKnownSources(k *Known) *EditTaskScene {
	a.known = k
	return a
}

func (a *EditTaskScene) SetCallbackOpenNote(f func(init string, callback func(s string))) *EditTaskScene {
	a.callbackOpenNote = f
	return a
}

func (a *EditTaskScene) LightOnNoteEditButton() {
	a.pNoteEditButton.SetText("[black:lime]Edit")
}

func (a *EditTaskScene) LightOffNoteEditButton() {
	a.pNoteEditButton.SetText("[-:gray]Edit")
}
