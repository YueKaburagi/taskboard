
package main

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"code.rocketnine.space/tslocum/cbind"
)

type TaskListScene struct {
	view *TreeView
	frame *tview.Frame
	book *tview.Pages
	dialog *tview.InputField

	store *TaskStore
	known *Known
	cfg *Configuration
	
	lastCursor *TreeNode

	switchToAddNewTaskScene func(*Task)
	callbackQuit func()

	actions map[string]Action
	keymap *cbind.Configuration
}

type Action struct {
	Key string
	Description string
	Body func(ev *tcell.EventKey) *tcell.EventKey
}

const (
	akQuit = "quit"
	akSwitchModeSelect = "switchModeSelect"
	akMoveCursorR = "moveCursorRight"
	akMoveCursorL = "moveCursorLeft"
	akSetTaskDone = "setTaskDone"
	akDeleteTask = "deleteTask"
	akEditTask = "editTask"
	akAddFilter = "addFilter"
	akExitMode = "exitMode"
)

func (l *TaskListScene) exitSelectionMode() {
	cNode := l.view.CurrentNode()
	if cNode != nil {
		l.lastCursor = cNode
		l.view.SetCurrentNode(nil)
	}
}

func (l *TaskListScene) genActions() map[string]Action {

	actions := make(map[string]Action)
	actions[akQuit] = Action{
		Key: akQuit,
		Description: "Quit taskboard.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			l.callbackQuit()
			return nil
		},
	}
	actions[akSwitchModeSelect] = Action{
		Key: akSwitchModeSelect,
		Description: "Toggle mode 'select'.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			cNode := l.view.CurrentNode()
			if cNode == nil {
				if l.lastCursor == nil {
					l.view.SetCurrentNode(l.view.Root)
				} else {
					// lastCursor が visibleかどうかはチェックしてないので不都合がある
					l.view.SetCurrentNode(l.lastCursor)
				}
			} else {
				l.view.SetCurrentNode(nil)
			}
			return nil
		},
	}
	actions[akMoveCursorR] = Action{
		Key: akMoveCursorR,
		Description: "Move cursor to right if it shown.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			cNode := l.view.CurrentNode()
			if cNode != nil {
				withWrappedTask(cNode, func(wt *WrappedTask){
					wt.SetCursor(wt.CursorSucc())
					cNode.Text = wt.GenDecolatedText()
				})
				return nil
			}
			return ev
		},
	}
	actions[akMoveCursorL] = Action{
		Key: akMoveCursorL,
		Description: "Move cursor to left if it shown.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			cNode := l.view.CurrentNode()
			if cNode != nil {
				withWrappedTask(cNode, func(wt *WrappedTask){
					wt.SetCursor(wt.CursorPred())
					cNode.Text = wt.GenDecolatedText()
				})
				return nil
			}
			return ev
		},
	}
	actions[akSetTaskDone] = Action{
		Key: akSetTaskDone,
		Description: "Set status done selected task.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			cNode := l.view.CurrentNode()
			if cNode != nil {
				withWrappedTask(cNode, func(wt *WrappedTask){
					task := wt.Task
					switch task.Type {
					case TtInfinity:
						if l.store != nil {
							task.SetDone()
							l.SetTask(task, task) // Refresh task item.
						} else {
							logger.Printf("[Warn][ak:%s] TaskListScene has no TaskStore. \n", akSetTaskDone)
						}
					default:
						task.SetDone()
						l.view.Root.RemoveChild(cNode)
					}
				})
				return nil
			}
			return ev
		},
	}
	actions[akEditTask] = Action{
		Key: akEditTask,
		Description: "Edit selected task",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			cNode := l.view.CurrentNode()
			if l.switchToAddNewTaskScene != nil {
				if cNode == nil {
					l.switchToAddNewTaskScene(nil)
				} else {
					withWrappedTask(cNode, func(wt *WrappedTask){
						l.switchToAddNewTaskScene(wt.Task)
					})
				}
			return nil
			}
			return ev
		},
	}
	actions[akAddFilter] = Action{
		Key: akAddFilter,
		Description: "Change mode into 'add-filter'.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			l.dialog.SetText("")
			l.book.AddAndSwitchToPage("dialog", l.dialog, true)
			return nil
		},
	}
	actions[akExitMode] = Action{
		Key: akExitMode,
		Description: "Exit mode.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			l.exitSelectionMode()
			return nil
		},
	}
	actions[akDeleteTask] = Action{
		Key: akDeleteTask,
		Description: "Delete selected task.",
		Body: func(ev *tcell.EventKey) *tcell.EventKey {
			cNode := l.view.CurrentNode()
			withWrappedTask(cNode, func(wt *WrappedTask){
				l.store.RemoveTask(wt.Task)
			})
			return nil
		},
	}


	return actions
}

func (l *TaskListScene) setKeymap() {
	l.keymap.Clear()
	
	l.keymap.SetRune(tcell.ModNone, 'q', l.actions[akQuit].Body)
	l.keymap.SetRune(tcell.ModNone, 'v', l.actions[akSwitchModeSelect].Body)
	l.keymap.SetRune(tcell.ModNone, 'l', l.actions[akMoveCursorR].Body)
	l.keymap.SetRune(tcell.ModNone, 'h', l.actions[akMoveCursorL].Body)
	l.keymap.SetRune(tcell.ModNone, 'd', l.actions[akSetTaskDone].Body)
	l.keymap.SetRune(tcell.ModNone, 'R', l.actions[akDeleteTask].Body)
	l.keymap.SetKey(tcell.ModNone, tcell.KeyEnter, l.actions[akEditTask].Body)
	l.keymap.SetRune(tcell.ModNone, 'f', l.actions[akAddFilter].Body)
	l.keymap.SetKey(tcell.ModNone, tcell.KeyEscape, l.actions[akExitMode].Body)
}

func (l *TaskListScene) resetFrameHeader() {
	s := " [silver]Tasks"

	if l.store != nil && l.store.Filters() != nil {
		for _, fl := range l.store.Filters() {
			s = s + " " + ToStringAsIndicatorStyle(fl)
		}
	}

	l.frame.Clear()
	l.frame.AddText(s, true, tview.AlignLeft, tcell.ColorBlack)
}

func NewTaskListScene() *TaskListScene {
	pView := NewTreeView()
	pView.Root = NewTreeNode("[silver]---[-]")
	pView.IndentSize = 2
	pView.IsDrawRoot = false
	pFrame := tview.NewFrame(pView).
		SetBorders(0,0,0,0,0,0)
	pPages := tview.NewPages().AddPage("base", pFrame, true, true)
	pDialog := tview.NewInputField().SetLabel("Filter by Tag: ")

	l := &TaskListScene{
		view: pView,
		frame: pFrame,
		book: pPages,
		dialog: pDialog,
		store: nil,
		lastCursor: nil,
		switchToAddNewTaskScene: nil,
		callbackQuit: func(){},
		keymap: cbind.NewConfiguration(),
	}
	l.resetFrameHeader()
	l.actions = l.genActions()


	pDialog.SetAutocompleteFunc(func (str string) []string {
		if l.known != nil {
			return l.known.HandlerAutocompleteTag(str)
		} else {
			return nil
		}
	})
	pDialog.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyEnter:
			s := pDialog.GetText()
			filter := TaskFilter{FlType: TlfTag, FlKey: s}
			if s != "" {
				l.store.AddFilter(filter)
			} else {
				l.store.ClearFilters()
			}
			l.resetFrameHeader()
			pPages.ShowPage("base")
			pPages.RemovePage("dialog")
		case tcell.KeyEscape:
			pPages.ShowPage("base")
			pPages.RemovePage("dialog")
		}
	})



	l.setKeymap()
	pView.SetInputCapture(l.keymap.Capture)

	return l
}

func (l *TaskListScene) SetKnown(k *Known) *TaskListScene {
	l.known = k
	return l
}

func withWrappedTask(node *TreeNode, delegate func(wt *WrappedTask)) {
	p := node.Reference
	switch p.(type) {
	case *WrappedTask:
		delegate(p.(*WrappedTask))
	}
}

func (l *TaskListScene) GetScene() tview.Primitive {
	return l.book
}

func (l *TaskListScene) SetTaskStore(store *TaskStore) *TaskListScene {
	l.store = store

	l.store.AddHookViewChanged(l.RefreshTaskList)

	view := l.store.View()
	l.RefreshTaskList(view)
	return l
}

func (l *TaskListScene) RefreshTaskList(view []*Task) {
	l.view.Root.ClearChildren()

	for _, t := range view {
		if t != nil {
			l.addTaskItem(NewWrappedTask(l.cfg, t))
		}
	}
}

func (l *TaskListScene) SetSwitchToAddNewTaskScene(callback func(t *Task)) *TaskListScene {
	l.switchToAddNewTaskScene = callback
	return l
}

func (l *TaskListScene) SetQuitCallback(callback func()) *TaskListScene {
	l.callbackQuit = callback
	return l
}

func (l *TaskListScene) SetConfiguration(cfg *Configuration) *TaskListScene {
	l.cfg = cfg
	return l
}

func (l *TaskListScene) addTaskItem(wt *WrappedTask) {
	newnode := NewTreeNode(wt.GenDecolatedText())
	newnode.Reference = wt
	newnode.AddHookBlur(func(itself *TreeNode) {
		itself.Text = wt.GenDecolatedText()
	})
	newnode.AddHookSelect(func(itself *TreeNode) {
		itself.Text = "[-:blue]" + wt.GenDecolatedText()
		l.lastCursor = itself
	})
	l.view.Root.AddLastChild(newnode)
}

func (l *TaskListScene) AddTask(task *Task) *TaskListScene {
	l.store.AddTask(task)
	return l
}

func (l *TaskListScene) SetTask(placeAt *Task, newTask *Task) *TaskListScene {
	l.store.ReplaceTask(placeAt, newTask)
	return l
}
