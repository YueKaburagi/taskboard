
package main

import (
	"k8s.io/utils/integer"
)

type ColoredBlock struct {
	Text string
	FGColor string
	Style string
	Reference interface{}
}

type BlockManager struct {
	blocks []ColoredBlock
	cursor int
}

func NewBlockManager() *BlockManager {
	return &BlockManager{ blocks: nil, cursor: -1 }
}

func (m *BlockManager) SetBlocks(blocks []ColoredBlock) *BlockManager {
	m.blocks = blocks
	return m
}

func (w *BlockManager) GenDecolatedText() string {
	s := ""
	for i, b := range w.blocks {
		color := b.FGColor
		var suffix string
		if b.Style == "" {
			suffix = "]"
		} else {
			suffix = ":" + b.Style + "]"
		}
		if i == w.cursor {
			color = "black" + ":" + color
		} else {
			color = color + ":" + "black"
		}
		s = s + "[" + color + suffix + b.Text + "[-:black:-] "
	}
	return s
}

func (m *BlockManager) SetCursor(cursor int) *BlockManager {
	m.cursor = cursor
	m.cursor = integer.IntMin(m.cursor, len(m.blocks) -1)
	m.cursor = integer.IntMax(m.cursor, -1)
	return m
}

func (m *BlockManager) GetCursor() int {
	return m.cursor
}

func (m *BlockManager) UseCurrentReference(delegate func(ref interface{})) bool {
	if m.cursor == -1 {
		return false
	} else {
		delegate(m.blocks[m.cursor].Reference)
		return true
	}
}

func (m *BlockManager) CursorSucc() int {
	succ := m.cursor + 1
	if succ >= len(m.blocks) {
		succ = 0
	}
	return succ
}

func (m *BlockManager) CursorPred() int {
	pred := m.cursor - 1
	if pred < 0 {
		pred = len(m.blocks) - 1
	}
	return pred
}

