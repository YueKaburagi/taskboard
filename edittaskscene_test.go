
package main

import (
	"testing"
	"time"
	"github.com/stretchr/testify/assert"
)


func TestParseTime_Patterns(t *testing.T) {

	now := time.Now()

	genfut := func (str string) assert.Comparison {
		rt := ParseTime(str)
		if rt != nil {
			return func() bool {return now.Before(*rt)}
		} else {
			return func() bool {return false}
		}
	}
	genpred := func (str string, pred func (x time.Time) bool) assert.Comparison {
		rt := ParseTime(str)
		if rt != nil {
			return func() bool {return pred(*rt)}
		} else {
			return func() bool {return false}
		}
	}

	assert.Condition(t, genfut("5/31"))
	assert.Condition(t, genpred("5/31", func(x time.Time) bool {return 5 == x.Month() }))
	assert.Condition(t, genpred("5/31", func(x time.Time) bool {return 31 == x.Day() }))
	assert.Condition(t, genfut("3/8 8:"))
	assert.Condition(t, genpred("3/8 8:", func(x time.Time) bool {return 8 == x.Hour() }))
	assert.Condition(t, genfut("11/1 1:11"))
	assert.Condition(t, genpred("11/1 1:11", func(x time.Time) bool {return 11 == x.Minute() }))
	assert.Condition(t, genpred("1990/7/12", func(x time.Time) bool {return 1990 == x.Year() }))
}
