
package main

import (
	"os"
	"sort"
	"encoding/json"
)

type TaskStore struct {
	Tasks []*Task
	LastIndex int

	filters []TaskFilter
	view []*Task
	hooksViewChanged []func(view []*Task)
	hooksTasksChanged []func(tasks []*Task)
}

func LoadTaskStore(filename string) (*TaskStore, error) {
	file, err := os.OpenFile(filename, os.O_RDONLY, 0644)
	if err != nil {
		if os.IsNotExist(err) {
			return &TaskStore{ Tasks: nil }, nil
		} else {
			return nil, err
		}
	} else {
		var store TaskStore
		decoder := json.NewDecoder(file)
		err := decoder.Decode(&store)
		store.callHooksTasksChanged()
		return &store, err
	}
}

func (s *TaskStore) RefreshTags(k *Known) {
	for _, task := range s.Tasks {
		for _, tag := range task.Tags {
			alt := k.LookupOrCreateTag(tag.Name)
			tag.AlterName = alt.AlterName
		}
	}
}

func (s *TaskStore) SaveTo(filename string) error {
	file, err := os.OpenFile(filename, os.O_WRONLY | os.O_CREATE | os.O_TRUNC, 0644)
	if err != nil {
		LogInfo(LLMinimum, "Cannot open file. [" + filename + "].")
		logger.Panic(err)
	}
	encoder := json.NewEncoder(file)
	return encoder.Encode(s)
}




func (s *TaskStore) AddHookViewChanged(hook func(view []*Task)) {
	s.hooksViewChanged = append(s.hooksViewChanged, hook)
}

func (s *TaskStore) callHooksViewChanged() {
	for _, f := range s.hooksViewChanged {
		if f != nil {
			f(s.view)
		}
	}
}

func (s *TaskStore) AddHookTasksChanged(hook func(tasks []*Task)) {
	s.hooksTasksChanged = append(s.hooksTasksChanged, hook)
}

func (s *TaskStore) callHooksTasksChanged() {
	for _, f := range s.hooksTasksChanged {
		if f != nil {
			f(s.Tasks)
		}
	}

	s.applyFilter()
}


func (s *TaskStore) applyFilter() {
	itr := 0
	view := make([]*Task, len(s.Tasks))
	for _, t := range s.Tasks {
		if t != nil {
			all := true
			for _, fl := range s.filters {
				if all {
					all = GenFilter(fl)(*t)
				} else {
					break
				}
			}
			if t.State != StateNormal {
				all = false
			}
			if all {
				view[itr] = t
				itr = itr + 1
			}
		}
	}

	s.view = view

	s.sort()
	s.callHooksViewChanged()
}




func (s *TaskStore) GetNextIndex() int {
	s.LastIndex = s.LastIndex + 1
	return s.LastIndex
}



func (s *TaskStore) AddTask(task *Task) {
	if task != nil {
		s.Tasks = append(s.Tasks, task)
		// NOTE: append retruns new instance.
		s.callHooksTasksChanged()
	}
}

func (s *TaskStore) ReplaceTask(old *Task, new *Task) {
	if old == nil {
		logger.Println("[TaskStore/ReplaceTask] Replace cancelled: Old task is nil.")
		return
	} else if new != nil {
		for i, t := range s.Tasks {
			if t == old {
				s.Tasks[i] = new
				s.callHooksTasksChanged()
				return
			}
		}
	}
	logger.Printf("[TaskStore/ReplaceTask] Replace cancelled: Old task is not found.\nOld task name: %s\n", old.Name)
}

func (s *TaskStore) RemoveTask(task *Task) {
	itr := -1
	for i, t := range s.Tasks {
		if t == task {
			itr = i
			break
		}
	}

	if itr != -1 {
		copy(s.Tasks[itr:], s.Tasks[itr+1:])
		length := len(s.Tasks)
		s.Tasks[length-1] = nil
		s.Tasks = s.Tasks[:length-1]
		s.callHooksTasksChanged()
		return
	}
	logger.Println("[TaskStore/RemoveTask] Remove cancelled: No such task.")
}



func (s *TaskStore) AddFilter(filter TaskFilter) {
	isUnique := true

	for _, fl := range s.filters {
		if filter.FlType == fl.FlType {
			switch fl.FlType {
			case TlfTag:
				if fl.FlKey == filter.FlKey {
					isUnique = false
					break
				}
			}
		}
	}
	if isUnique {
		s.filters = append(s.filters, filter)
		s.applyFilter()
	}
}

func (s *TaskStore) ClearFilters() {
	if s.filters != nil {
		s.filters = nil
		s.applyFilter()
	}
}

func (s *TaskStore) View() []*Task {
	return s.view
}

func (s *TaskStore) Filters() []TaskFilter {
	return s.filters
}



type SortKey int

const (
	SortCreatedTime SortKey = iota
	SortPriority 
)

type ord int
const (
	LT ord = -1
	EQ = iota
	GT = 1
)

func (s *TaskStore) sort() {
	sortKeys := []SortKey{SortPriority, SortCreatedTime}

	genSorter := func(order SortKey) func (i,j int) ord {
		switch order {
		case SortPriority:
			return func(i,j int) ord {
				iLv := 99999999
				jLv := 99999999
				if s.view[i] != nil && s.view[i].Priority != nil {
					iLv = int(s.view[i].Priority.Level)
				}
				if s.view[j] != nil && s.view[j].Priority != nil {
					jLv = int(s.view[j].Priority.Level)
				}

				if iLv > jLv {
					return GT
				} else if iLv < jLv {
					return LT
				} else {
					return EQ
				}
			}
		case SortCreatedTime:
			return func(i,j int) ord {
				if s.view[i].CreatedTime.Before(s.view[j].CreatedTime) {
					return LT
				} else if s.view[i].CreatedTime.After(s.view[j].CreatedTime) {
					return GT
				} else {
					return EQ
				}
			}
		}
		return func(i,j int) ord {return EQ}
	}

	sort.Slice(s.view, func(i,j int) bool {
		for _, sk := range sortKeys {
			sorter := genSorter(sk)
			switch sorter(i,j) {
			case LT:
				return true
			case GT:
				return false
			default:
				continue
			}
		}
		return false
	})
}
