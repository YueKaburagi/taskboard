module albormentum.com/taskboard

go 1.16

require (
	code.rocketnine.space/tslocum/cbind v0.1.5
	github.com/gdamore/tcell/v2 v2.2.1
	github.com/rivo/tview v0.0.0-20210427112837-09cec83b1732
	github.com/stretchr/testify v1.7.0
	k8s.io/utils v0.0.0-20210305010621-2afb4311ab10
)
