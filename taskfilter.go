
package main

import ()

type TlFilter int

const (
	TlfUndefined TlFilter = iota
	TlfTag
	TlfName
)

func (tlf TlFilter) String() string {
	switch tlf {
	case TlfTag:
		return "Tag"
	case TlfName:
		return "Name"
	default:
		return "Undefined"
	}
}



type TaskFilter struct {
	FlType TlFilter
	FlKey string
}

func ToStringAsIndicatorStyle(f TaskFilter) string {
	prefix := ""
	switch f.FlType {
	case TlfTag:
		prefix = "t:"
	case TlfName:
		prefix = "n:"
	}
	return prefix + f.FlKey
}

func GenFilter(fl TaskFilter) func(t Task) bool {
	switch fl.FlType {
	case TlfTag:
		return func(t Task) bool {return t.HasTag(fl.FlKey)}
	case TlfName:
		return func(t Task) bool {return t.Name == fl.FlKey}
	default:
		return func(t Task) bool {return false}
	}
}


