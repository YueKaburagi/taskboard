
package main

import ()

type Configuration struct {
	DefaultPriorityLevel PriorityLevel
	defaultPriorityCache *Priority

	NoteIcon rune
}

func NewMockConfiguration() *Configuration {
	return &Configuration{
		DefaultPriorityLevel: 2,

		NoteIcon: '⏍',
	}
}

func (c *Configuration) GetDefaultPriority(known *Known) *Priority {
	if c.defaultPriorityCache == nil {
		for _, p := range known.Priorities {
			if p.Level == c.DefaultPriorityLevel {
				c.defaultPriorityCache = p
			}
		}
	}
	return c.defaultPriorityCache
}

